1. Membuat Database
create database myshop;

2. Membuat table
use myshop;

create table users(
	id int primary key auto_increment,
	name varchar(255),
    email varchar(255),
    password varchar(255)
);

create table categories(
	id int primary key auto_increment,
    name varchar(255)
);

create table items(
	id int primary key auto_increment,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    foreign key (category_id) references categories(id)
);

3. Memasukkan data
insert into users(name, email, password) 
values ("John Doe", "john@doe.com", "john123"),
	   ("Jane Doe", "jane@doe.com", "jenita123");

insert into categories(name) 
values ("gadget"),
	   ("cloth"),
       ("men"),
       ("women"),
       ("branded");
       
insert into items(name, description, price, stock, category_id) 
values ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
	   ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
       ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
       
4. Mengambil data
a. users
select name, email from users;

b. items
select * from items where price > 1000000;
select * from items where name like '%uniklo%';

c. items join dengan categories
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items
inner join categories on categories.id = items.category_id;

5. Mengubah database
update items set price = 2500000 where name = "Sumsang b50";